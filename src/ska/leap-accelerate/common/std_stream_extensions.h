/**
 * ICRAR - International Centre for Radio Astronomy Research
 * (c) UWA - The University of Western Australia
 * Copyright by UWA(in the framework of the ICRAR)
 * All rights reserved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#pragma once

#include <iostream>
#include <iomanip>
#include <vector>
#include <set>
#include <map>
#include <optional>

namespace std {
    /**
     * @brief Stream out a set of values.
     * 
     * @tparam T streamable type
     * @param os output stream
     * @param values set of values
     * @return std::ostream&
     */
    template <typename T>
    std::ostream& operator<<(std::ostream& os, const std::set<T>& values)
    {
        os << "{"; 
        for (const auto& value : values)
        {
            os << value << ", "; 
        } 
        os << "}\n"; 
        return os;
    }

    /**
     * @brief Stream out a mapping of key-value pairs.
     * 
     * @tparam T streamable key type
     * @tparam S streamable value type
     * @param os output stream
     * @param map map of keys->values
     * @return std::ostream& 
     */
    template <typename T, typename S> 
    std::ostream& operator<<(std::ostream& os, const std::map<T, S>& map)
    { 
        for (const auto& key_values : map)
        {
            os << key_values.first << " : "
            << key_values.second << "\n";
        }
        return os; 
    }

    /**
     * @brief Stream out an optional value. Based on:
     * https://www.boost.org/doc/libs/1_72_0/boost/optional/optional_io.hpp
     * 
     * @tparam T streamable type
     * @param os output stream
     * @param opt optional value
     * @return std::ostream& 
     */
    template <typename T>
    std::ostream& operator<<(std::ostream& os, const std::optional<T>& opt)
    {
        if (os.good())
        {
            if(opt.has_value())
            {
                os << ' ' << opt.value();
            }
            else
            {
                os << "--";
            }
        }
        return os;
    }

    /**
     * @brief Stream in an optional value. Based on:
     * https://www.boost.org/doc/libs/1_72_0/boost/optional/optional_io.hpp
     * 
     * @tparam T streamable type
     * @param is input stream
     * @param opt optional value
     * @return std::istream& 
     */
    template <typename T>
    inline std::istream& operator>>(std::istream& is, std::optional<T>& opt)
    {
        if(is.good())
        {
            int d = is.get();
            if(d == ' ')
            {
                T x;
                is >> x;
                opt = std::move(x);
            }
            else
            {
                if(is.get() == '-' && is.get() == '-')
                {
                    opt = std::nullopt;
                }
                else
                {
                    is.setstate( std::ios::failbit );
                }
            }
        }
        return is;
    }
} // namespace std
