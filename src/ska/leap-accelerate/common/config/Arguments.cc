/**
 * ICRAR - International Centre for Radio Astronomy Research
 * (c) UWA - The University of Western Australia
 * Copyright by UWA(in the framework of the ICRAR)
 * All rights reserved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "Arguments.h"

#include <ska/leap-accelerate/cuda/cuda_info.h>
#include <ska/leap-accelerate/ms/MeasurementSet.h>
#include <ska/leap-accelerate/exception/exception.h>

#include <rapidjson/document.h>
#include <rapidjson/istreamwrapper.h>
#include <rapidjson/ostreamwrapper.h>

namespace ska
{
    /**
     * Default set of command line interface arguments
     */
    CLIArgumentsDTO CLIArgumentsDTO::GetDefaultArguments()
    {
        auto args = CLIArgumentsDTO();
        args.inputType = "file";
        args.filePath = std::nullopt;
        args.configFilePath = std::nullopt;
        args.streamOutType = "single";
        args.outputFilePath = std::nullopt;

        args.stations = std::nullopt;
        args.referenceAntenna = std::nullopt;
        args.directions = std::nullopt;
        args.computeImplementation = "cpu";
        args.solutionInterval = "[0,null,null]"; // Average over all timesteps
        args.readAutocorrelations = true;
        args.minimumBaselineThreshold = 0.0;
        args.mwaSupport = false;
        args.computeCal1 = true;
        args.useFileSystemCache = true;
        //args.useIntermediateBuffer = determined from device memory
        //args.useCusolver = determined from device memory
        args.verbosity = static_cast<int>(log::DEFAULT_VERBOSITY);
        return args;
    }

    ArgumentsDTO::ArgumentsDTO(CLIArgumentsDTO&& args)
        : filePath(std::move(args.filePath))
        , configFilePath(std::move(args.configFilePath))
        , outputFilePath(std::move(args.outputFilePath))
        , stations(args.stations)
        , referenceAntenna(args.referenceAntenna)
        , minimumBaselineThreshold(args.minimumBaselineThreshold)
        , readAutocorrelations(args.readAutocorrelations)
        , mwaSupport(args.mwaSupport)
        , computeCal1(args.computeCal1)
        , useFileSystemCache(args.useFileSystemCache)
        , useIntermediateBuffer(args.useIntermediateBuffer)
        , useCusolver(args.useCusolver)
    {
        //Perform type conversions
        if(args.inputType.has_value())
        {
            inputType = InputType(); //Default value ignored
            if(!TryParseInputType(args.inputType.value(), inputType.value()))
            {
                throw std::invalid_argument("invalid compute implementation argument");
            }
        }
        
        if(args.computeImplementation.has_value())
        {
            computeImplementation = ComputeImplementation(); //Default value ignored
            if(!TryParseComputeImplementation(args.computeImplementation.value(), computeImplementation.value()))
            {
                throw std::invalid_argument("invalid compute implementation argument");
            }
        }

        if(args.streamOutType.has_value())
        {
            streamOutType = StreamOutType();
            if(!TryParseStreamOutType(args.streamOutType.value(), streamOutType.value()))
            {
                throw ska::invalid_argument_exception("invalid stream out type argument", args.streamOutType.value(), __FILE__, __LINE__);
            }
        }

        if(args.directions.has_value())
        {
            directions = ParseDirections(args.directions.value());
        }

        if(args.solutionInterval.has_value())
        {
            solutionInterval = ParseSlice(args.solutionInterval.value());
        }

        if(args.verbosity.has_value())
        {
            verbosity = static_cast<ska::log::Verbosity>(args.verbosity.value());
        }
    }

    Arguments::Arguments(ArgumentsDTO&& cliArgs)
    : m_inputType(InputType::file)
    , m_streamOutType()
    , m_computeImplementation(ComputeImplementation::cpu)
    , m_solutionInterval()
    , m_minimumBaselineThreshold(0)
    , m_mwaSupport(false)
    , m_computeCal1(true)
    , m_verbosity(ska::log::Verbosity::trace)
    , m_computeOptions()
     //Initial values are overwritten
    {
        // Initialize default arguments first
        OverrideArguments(CLIArgumentsDTO::GetDefaultArguments());

        // Read the config argument second and apply the config arguments over the default arguments
        if(cliArgs.configFilePath.has_value())
        {
            // Configuration via json config
            OverrideArguments(ParseConfig(cliArgs.configFilePath.value()));
        }

        // Override the config args with the remaining cli arguments
        OverrideArguments(std::move(cliArgs));
        Validate();

        // Load resources
        ska::log::Initialize(GetVerbosity());
        switch (m_inputType)
        {
        case InputType::file:
            if (m_filePath.has_value())
            {
                m_measurementSet = std::make_unique<MeasurementSet>(m_filePath.value());
            }
            else
            {
                throw std::invalid_argument("measurement set filename not provided");
            }
            break;
        case InputType::stream:
            throw std::runtime_error("only measurement set input is currently supported");
            break;
        default:
            throw std::runtime_error("only measurement set input is currently supported");
            break;
        }
    }

    void Arguments::OverrideArguments(ArgumentsDTO&& args)
    {
        if(args.inputType.has_value())
        {
            m_inputType = args.inputType.value();
        }

        if(args.filePath.has_value())
        {
            m_filePath = args.filePath.value();
        }

        if(args.configFilePath.has_value())
        {
            m_configFilePath = args.configFilePath.value();
        }

        if(args.streamOutType.has_value())
        {
            m_streamOutType = args.streamOutType.value();
        }

        if(args.outputFilePath.has_value())
        {
            m_outputFilePath = args.outputFilePath.value();
        }

        if(args.referenceAntenna.has_value())
        {
            m_referenceAntenna = args.referenceAntenna.value();
        }

        if(args.directions.has_value())
        {
            m_directions = args.directions.value();
        }

        if(args.computeImplementation.has_value())
        {
            m_computeImplementation = args.computeImplementation.value();
        }

        if(args.solutionInterval.has_value())
        {
            m_solutionInterval = args.solutionInterval.value();
        }

        if(args.minimumBaselineThreshold.has_value())
        {
            m_minimumBaselineThreshold = args.minimumBaselineThreshold.value();
        }

        if(args.mwaSupport.has_value())
        {
            m_mwaSupport = args.mwaSupport.value();
        }

        if(args.computeCal1.has_value())
        {
            m_computeCal1 = args.computeCal1.value();
        }

        if(args.useFileSystemCache.has_value())
        {
            m_computeOptions.isFileSystemCacheEnabled = args.useFileSystemCache.value();
        }

        if(args.useIntermediateBuffer.has_value())
        {
            m_computeOptions.useIntermediateBuffer = args.useIntermediateBuffer.value();
        }

        if(args.useCusolver.has_value())
        {
            m_computeOptions.useCusolver = args.useCusolver.value();
        }

        if(args.verbosity.has_value())
        {
            m_verbosity = args.verbosity.value();
        }
    }


    void Arguments::Validate() const
    {
        if(m_directions.size() == 0)
        {
            throw std::invalid_argument("directions argument not provided");
        }
    }

    std::optional<std::string> Arguments::GetOutputFilePath() const
    {
        return m_outputFilePath;
    }

    StreamOutType Arguments::GetStreamOutType() const
    {
        return m_streamOutType;
    }

    std::unique_ptr<std::ostream> Arguments::CreateOutputStream(double startEpoch) const
    {
        if(!m_outputFilePath.has_value())
        {
            return std::make_unique<std::ostream>(std::cout.rdbuf());
        }
        if(m_streamOutType == StreamOutType::collection)
        {
            return std::make_unique<std::ostream>(std::cout.rdbuf());
        }
        else if(m_streamOutType == StreamOutType::singleFile)
        {
            auto path = m_outputFilePath.value();
            return std::make_unique<std::ofstream>(path);
        }
        else if(m_streamOutType == StreamOutType::multipleFiles)
        {
            auto path = m_outputFilePath.value() + "." + std::to_string(startEpoch) + ".json";
            return std::make_unique<std::ofstream>(path);
        }
        else
        {
            throw std::runtime_error("invalid output stream type");
        }
    }

    const MeasurementSet& Arguments::GetMeasurementSet() const
    {
        return *m_measurementSet;
    }

    const std::vector<SphericalDirection>& Arguments::GetDirections() const
    {
        return m_directions;
    }

    ComputeImplementation Arguments::GetComputeImplementation() const
    {
        return m_computeImplementation;
    }

    Slice Arguments::GetSolutionInterval() const
    {
        return m_solutionInterval;
    }

    std::optional<unsigned int> Arguments::GetReferenceAntenna() const
    {
        return m_referenceAntenna;
    }

    double Arguments::GetMinimumBaselineThreshold() const
    {
        return m_minimumBaselineThreshold;
    }

    bool Arguments::ComputeCal1() const
    {
        return m_computeCal1;
    }

    ComputeOptionsDTO Arguments::GetComputeOptions() const
    {
        return m_computeOptions;
    } 

    ska::log::Verbosity Arguments::GetVerbosity() const
    {
        return m_verbosity;
    }

    ArgumentsDTO Arguments::ParseConfig(const std::string& configFilepath)
    {
        ArgumentsDTO args;
        ParseConfig(configFilepath, args);
        return args;
    }

    template<typename T> // rapidjson::GenericObject
    bool SafeGetBoolean(const T& object, const std::string& message, const std::string& file, int line)
    {
        if(object.value.IsBool())
        {
            return object.value.GetBool();
        }
        else
        {
            throw json_exception(message, file, line);
        }
    }

    void Arguments::ParseConfig(const std::string& configFilepath, ArgumentsDTO& args)
    {
        auto ifs = std::ifstream(configFilepath);
        rapidjson::IStreamWrapper isw(ifs);
        rapidjson::Document doc;
        doc.ParseStream(isw);

        if(!doc.IsObject())
        {
            throw json_exception("expected config to be an object", __FILE__, __LINE__);
        }
        for(auto it = doc.MemberBegin(); it != doc.MemberEnd(); ++it)
        {
            if(!it->name.IsString())
            {
                throw json_exception("config keys must be of type string", __FILE__, __LINE__);
            }
            else
            {
                std::string key = it->name.GetString();
                if(key == "inputType")
                {
                    //args.sourceType = it->value.GetInt(); //TODO: use string
                }
                else if(key == "filePath")
                {
                    args.filePath = it->value.GetString();
                    if(it->value.IsString())
                    {
                        args.filePath = it->value.GetString();
                    }
                    else
                    {
                        throw json_exception("filePath must be of type string", __FILE__, __LINE__);
                    }
                }
                else if(key == "configFilePath")
                {
                    throw json_exception("recursive config detected", __FILE__, __LINE__);
                }
                else if(key == "streamOutType")
                {
                    StreamOutType e = {};
                    if(TryParseStreamOutType(it->value.GetString(), e))
                    {
                        args.streamOutType = e;
                    }
                    else
                    {
                        throw json_exception("invalid stream out type string", __FILE__, __LINE__);
                    }
                }
                else if(key == "outputFilePath")
                {
                    if(it->value.IsString())
                    {
                        args.outputFilePath = it->value.GetString();
                    }
                    else
                    {
                        throw json_exception("outFilePath must be of type string", __FILE__, __LINE__);
                    }
                }
                else if(key == "stations")
                {
                    if(it->value.IsInt())
                    {
                        args.stations = it->value.GetInt();
                    }
                    else
                    {
                        throw json_exception("outFilePath must be of type int", __FILE__, __LINE__);
                    }
                }
                else if(key == "solutionInterval")
                {
                    if(it->value.IsArray())
                    {
                        args.solutionInterval = ParseSlice(it->value);
                    }
                }
                else if(key == "referenceAntenna")
                {
                    if(it->value.IsInt())
                    {
                        args.referenceAntenna = it->value.GetInt();
                    }
                    else
                    {
                        throw json_exception("referenceAntenna must be of type unsigned int", __FILE__, __LINE__);
                    }
                }
                else if(key == "directions")
                {
                    args.directions = ParseDirections(it->value);
                }
                else if(key == "computeImplementation")
                {
                    ComputeImplementation e = {};
                    if(TryParseComputeImplementation(it->value.GetString(), e))
                    {
                        args.computeImplementation = e;
                    }
                    else
                    {
                        throw json_exception("invalid compute implementation string", __FILE__, __LINE__);
                    }
                }
                else if(key == "minimumBaselineThreshold")
                {
                    if(it->value.IsDouble())
                    {
                        args.minimumBaselineThreshold = it->value.GetDouble();
                    }
                    else
                    {
                        throw json_exception("minimumBaselineThreshold must be of type double", __FILE__, __LINE__);
                    }
                }
                else if(key == "mwaSupport")
                {
                    args.mwaSupport = SafeGetBoolean(*it, "mwaSupport must be of type bool", __FILE__, __LINE__);
                }
                else if(key == "computeCal1")
                {
                    args.computeCal1 = SafeGetBoolean(*it, "computeCal1 must be of type bool", __FILE__, __LINE__);
                }
                else if(key == "autoCorrelations")
                {
                     args.readAutocorrelations = SafeGetBoolean(*it, "autoCorrelations must be of type bool", __FILE__, __LINE__);
                }
                else if(key == "useFileSystemCache")
                {
                     args.useFileSystemCache = SafeGetBoolean(*it, "useFileSystemCache must be of type bool", __FILE__, __LINE__);
                }
                else if(key == "useIntermediateBuffer")
                {
                    args.useIntermediateBuffer = SafeGetBoolean(*it, "useIntermediateBuffer must be of type bool", __FILE__, __LINE__);
                }
                else if(key == "useCusolver")
                {
                    args.useCusolver = SafeGetBoolean(*it, "useCusolver must be of type bool", __FILE__, __LINE__);
                }
                else if(key == "verbosity")
                {
                    if(it->value.IsInt())
                    {
                        args.verbosity = static_cast<log::Verbosity>(it->value.GetInt());
                    }
                    if(it->value.IsString())
                    {
                        log::Verbosity e = {};
                        if(TryParseVerbosity(it->value.GetString(), e))
                        {
                            args.verbosity = e;
                        }
                        else
                        {
                            throw json_exception("invalid verbosity string", __FILE__, __LINE__);
                        }
                    }
                    else
                    {
                        throw json_exception("verbosity must be of type int or string", __FILE__, __LINE__);
                    }
                }
                else
                {
                    std::stringstream ss;
                    ss << "invalid config key: " << key; 
                    throw json_exception(ss.str(), __FILE__, __LINE__);
                }
            }
        }
    }
} // namespace ska