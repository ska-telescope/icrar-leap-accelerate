/**
 * ICRAR - International Centre for Radio Astronomy Research
 * (c) UWA - The University of Western Australia
 * Copyright by UWA(in the framework of the ICRAR)
 * All rights reserved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#pragma once

#include <ska/leap-accelerate/algorithm/ComputeOptionsDTO.h>
#include <ska/leap-accelerate/ms/MeasurementSet.h>
#include <ska/leap-accelerate/common/SphericalDirection.h>
#include <ska/leap-accelerate/common/Slice.h>
#include <ska/leap-accelerate/core/compute_implementation.h>
#include <ska/leap-accelerate/core/log/logging.h>
#include <ska/leap-accelerate/core/stream_out_type.h>
#include <ska/leap-accelerate/core/InputType.h>

#include <optional>
#include <vector>
#include <string>
#include <memory>
#include <fstream>
#include <iostream>

namespace ska
{
    /**
     * @brief Raw arguments received via the command line interface using boost::program_options.
     * Only raw types std::string, bool, int, uint, float and double are allowed here. 
     * 
     */
    struct CLIArgumentsDTO
    {
        std::optional<std::string> inputType;
        std::optional<std::string> filePath;
        std::optional<std::string> configFilePath;

        std::optional<std::string> streamOutType;
        std::optional<std::string> outputFilePath;

        std::optional<int> stations;
        std::optional<unsigned int> referenceAntenna;
        std::optional<std::string> directions;
        std::optional<std::string> computeImplementation;
        std::optional<std::string> solutionInterval;
        std::optional<double> minimumBaselineThreshold;
        std::optional<bool> mwaSupport;
        std::optional<bool> computeCal1;
        std::optional<bool> readAutocorrelations;
        std::optional<int> verbosity;

        std::optional<bool> useFileSystemCache;
        std::optional<bool> useIntermediateBuffer;
        std::optional<bool> useCusolver;

        static CLIArgumentsDTO GetDefaultArguments();
    };

    /**
     * @brief Typed arguments of \c CLIArgumentsDTO 
     * 
     */
    struct ArgumentsDTO
    {
        ArgumentsDTO() = default;
        ArgumentsDTO(CLIArgumentsDTO&& args);

        std::optional<InputType> inputType; ///< MeasurementSet source type
        std::optional<std::string> filePath; ///< MeasurementSet filepath
        std::optional<std::string> configFilePath; ///< Optional config filepath
        std::optional<StreamOutType> streamOutType;
        std::optional<std::string> outputFilePath; ///< Calibration output file, print to terminal if empty

        std::optional<int> stations;
        std::optional<unsigned int> referenceAntenna;
        std::optional<std::vector<SphericalDirection>> directions;
        std::optional<ComputeImplementation> computeImplementation;
        std::optional<Slice> solutionInterval;
        std::optional<double> minimumBaselineThreshold;
        std::optional<bool> readAutocorrelations;
        std::optional<bool> mwaSupport;
        std::optional<bool> computeCal1;
        std::optional<ska::log::Verbosity> verbosity;
        
        std::optional<bool> useFileSystemCache; ///< Whether to update a file cache for fast inverse matrix loading
        std::optional<bool> useIntermediateBuffer; ///< Whether to allocate intermediate buffers for reduced cpu->gpu copies
        std::optional<bool> useCusolver; ///< Whether to use cusolverDn for matrix inversion
    };

    /**
     * Validated set of command line arguments required to perform leap calibration
     */
    class Arguments
    {
        /**
         * Constants
         */
        InputType m_inputType;
        std::optional<std::string> m_filePath; ///< MeasurementSet filepath
        std::optional<std::string> m_configFilePath; ///< Config filepath
        StreamOutType m_streamOutType;
        std::optional<std::string> m_outputFilePath; ///< Calibration output filepath

        std::optional<unsigned int> m_referenceAntenna; ///< Index of the reference antenna
        std::vector<SphericalDirection> m_directions; ///< Calibration directions
        ComputeImplementation m_computeImplementation; ///< Specifies the implementation for calibration computation
        Slice m_solutionInterval; ///< Specifies the interval to calculate solutions for
        double m_minimumBaselineThreshold; ///< Minimum baseline length otherwise flagged at runtime
        bool m_mwaSupport; ///< Negates baselines when enabled
        bool m_computeCal1; ///< Computes and uses Cal1 vector for solving ambiguities
        ska::log::Verbosity m_verbosity; ///< Defines logging level for std::out

        ComputeOptionsDTO m_computeOptions; ///< Defines options for compute performance tweaks
        
        /**
         * Resources
         */
        std::unique_ptr<MeasurementSet> m_measurementSet;

    public:
        Arguments(ArgumentsDTO&& cliArgs);

        /**
         * @brief Overrides the stored set of arguments.
         * 
         * @param args 
         */
        void OverrideArguments(ArgumentsDTO&& args);

        void Validate() const;

        std::optional<std::string> GetOutputFilePath() const;

        /**
         * @brief Using the outputFilePath member, creates an output stream object.
         * 
         * @param startEpoch 
         * @return std::unique_ptr<std::ostream> 
         */
        std::unique_ptr<std::ostream> CreateOutputStream(double startEpoch = 0.0) const;

        /**
         * @brief Gets the configuration for output stream type
         * 
         * @return StreamOutType 
         */
        StreamOutType GetStreamOutType() const;

        /**
         * @brief Gets the user specifified measurement set
         * 
         * @return MeasurementSet& 
         */
        const MeasurementSet& GetMeasurementSet() const;

        const std::vector<SphericalDirection>& GetDirections() const;

        ComputeImplementation GetComputeImplementation() const;

        Slice GetSolutionInterval() const;

        std::optional<unsigned int> GetReferenceAntenna() const;

        /**
         * @brief Gets the minimum baseline threshold in meteres. Baselines
         * of length beneath the threshold are to be filtered/flagged.
         * 
         * @return double baseline threshold length in meters
         */
        double GetMinimumBaselineThreshold() const;

        /**
         * @brief Whether cal1 should be computed.
         * 
         * @return true 
         * @return false 
         */
        bool ComputeCal1() const;

        /**
         * @brief Gets configured options related to compute performance
         * 
         * @return ComputeOptionsDTO
         */
        ComputeOptionsDTO GetComputeOptions() const;

        /**
         * @brief Gets the configured logging verbosity
         * 
         * @return ska::log::Verbosity 
         */
        ska::log::Verbosity GetVerbosity() const;

    private:
        /**
         * @brief Converts a JSON file to a config
         * 
         * @param configFilepath 
         * @return Config 
         */
        ArgumentsDTO ParseConfig(const std::string& configFilepath);
        
        /**
         * @brief Converts a JSON file to a config
         * 
         * @param configFilepath 
         * @param args 
         */
        void ParseConfig(const std::string& configFilepath, ArgumentsDTO& args);
    };
} // namespace ska
