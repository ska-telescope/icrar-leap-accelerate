/**
 * ICRAR - International Centre for Radio Astronomy Research
 * (c) UWA - The University of Western Australia
 * Copyright by UWA(in the framework of the ICRAR)
 * All rights reserved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#pragma once

#include <optional>

namespace ska
{
    /**
     * @brief Options received from I/O that optimizes computation performance based on input data and hardware configuration.
     * Can either be overriden by the user or intelligently determined at runtime if not set.
     */
    struct ComputeOptionsDTO
    {
        std::optional<bool> isFileSystemCacheEnabled; ///< Enables caching of expensive calculations to the filesystem
        std::optional<bool> useIntermediateBuffer;
        std::optional<bool> useCusolver;

        bool IsInitialized() const
        {
            return isFileSystemCacheEnabled.has_value()
            && useIntermediateBuffer.has_value()
            && useCusolver.has_value(); 
        }
    };
} // namespace ska