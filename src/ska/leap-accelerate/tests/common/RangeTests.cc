/**
 * ICRAR - International Centre for Radio Astronomy Research
 * (c) UWA - The University of Western Australia
 * Copyright by UWA(in the framework of the ICRAR)
 * All rights reserved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <gtest/gtest.h>

#include <ska/leap-accelerate/common/Slice.h>
#include <ska/leap-accelerate/common/Range.h>

namespace ska
{
    class RangeTests : public testing::Test
    {
    public:
        void TestConstructors()
        {
            using namespace std;

            ASSERT_NO_THROW(Slice());
            ASSERT_NO_THROW(Slice().Evaluate(1));
            ASSERT_THROW(Slice(0), ska::exception);
            ASSERT_NO_THROW(Slice(1).Evaluate(1));

            ASSERT_THROW(   Rangel( -1,   -1,   -1), ska::exception);
            ASSERT_THROW(   Rangel( -1,   -1,    0), ska::exception);
            ASSERT_THROW(   Rangel( -1,   -1,    1), ska::exception);
            ASSERT_THROW(   Rangel( -1,    0,   -1), ska::exception);
            ASSERT_THROW(   Rangel( -1,    0,    0), ska::exception);
            ASSERT_THROW(   Rangel( -1,    0,    1), ska::exception);
            ASSERT_THROW(   Rangel( -1,    1,   -1), ska::exception);
            ASSERT_THROW(   Rangel( -1,    1,    0), ska::exception);
            ASSERT_THROW(   Rangel( -1,    1,    1), ska::exception);

            ASSERT_NO_THROW(Slice( 0, nullopt, nullopt).Evaluate(1));
            ASSERT_THROW(   Slice( 0, nullopt,       0), ska::exception);
            ASSERT_NO_THROW(Slice( 0, nullopt,       1).Evaluate(1));
            ASSERT_NO_THROW(Slice( 0,       0, nullopt).Evaluate(1));
            ASSERT_THROW(   Slice( 0,       0,       0), ska::exception);
            ASSERT_THROW(   Slice( 0,       0,       1), ska::exception);
            ASSERT_NO_THROW(Slice( 0,       1, nullopt).Evaluate(1));
            ASSERT_THROW(   Slice( 0,       1,       0), ska::exception);
            ASSERT_NO_THROW(Slice( 0,       1,       1).Evaluate(1));

            ASSERT_NO_THROW(Slice( 1, nullopt, nullopt).Evaluate(1));
            ASSERT_THROW(   Slice( 1, nullopt,       0), ska::exception);
            ASSERT_NO_THROW(Slice( 1, nullopt,       1).Evaluate(1));
            ASSERT_THROW(   Slice( 1,       0, nullopt), ska::exception);
            ASSERT_THROW(   Slice( 1,       0,       0), ska::exception);
            ASSERT_THROW(   Slice( 1,       0,       1), ska::exception);
            ASSERT_NO_THROW(Slice( 1,       1, nullopt).Evaluate(1));
            ASSERT_THROW(   Slice( 1,       1,       0), ska::exception);
            ASSERT_THROW(   Slice( 1,       1,       1), ska::exception);
  
            ASSERT_THROW(   Slice(0, 1,  -2), ska::exception);
        }
    };

    TEST_F(RangeTests, TestConstructors) { TestConstructors(); }
} // namespace ska
