# Leap Accelerate CI Build Image

This image contains all required resources to build icrar-leap-accelerate from scratch. Use this image for faster CI builds. The build image can be rebuilt by manually triggering the `docker_ci_build_deploy` job in a gitlab pipeline.
