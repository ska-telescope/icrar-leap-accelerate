# Version History

## Unreleased

### Changed

* Changed from `icrar` folder and namespaces to `ska`

## [0.13.0]

### Added

* Added Python bindings, examples and tests
* Added Python CLI tool

## [0.10.0]

### Added

* Added intermediate buffer support
* Added compute options to cpu and cuda calibration implementations

## [0.8.0]

### Added

* Added solution interval support
* Added specifying reference antenna support
* Added cuda 8.0 compute targets to support Ampere graphics cards

## [0.7.3]

* Performance improvements to cpu and cuda calibration implementations
* Stability improvements

## [0.7.2]

### Changed

* Performance improvements to cuda calibration implementation

## [0.7.1]

### Added

* Added MWA calibration flag to swap baselines

## [0.7.0]

### Added

* Added filesystem caching of antenna matrices to cpu and cuda calibration implementations
* Added support for filtering short baselines

### Changed

* Changed to summing visibilities in complex domain
* Changed direction dependant matrix calculation
