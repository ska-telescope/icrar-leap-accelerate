"""Leap Gain Calibration streaming example."""

import asyncio
import logging
import os
from pathlib import Path

import casacore.tables as tables
import numpy as np
from aiokafka import AIOKafkaConsumer, AIOKafkaProducer
from astropy.coordinates import FK5, SkyCoord
from ska_sdp_datamodels.calibration import GainTable
from ska_sdp_datamodels.utilities import decode, encode

from leap.functional import leap_stream_gain_tables


__logger = logging.getLogger(__name__)


MWA_MS = Path(
    os.path.dirname(__file__),
    "../../testdata/mwa/1197638568-split.ms"
).resolve()


async def amain():
    ms_field = tables.table(str(MWA_MS/"FIELD"), readonly=True, ack=False)
    ms_phase_center: np.ndarray = ms_field.getcell("PHASE_DIR", 0)
    ms_units: str = ms_field.getcoldesc("PHASE_DIR")["keywords"]["QuantumUnits"]

    obs_phase_center = SkyCoord(ms_phase_center, frame=FK5, unit=ms_units)
    obs_directions = [
        SkyCoord(ra=7.09767229e-01, dec=-1.98773609e-04, frame=FK5, unit=ms_units),
        SkyCoord(ra=7.19767229e-01, dec=-1.28773609e-04, frame=FK5, unit=ms_units)
    ]

    bootstrap_servers = "localhost:9092"
    topic = "leap-gains"
    async with (
        AIOKafkaProducer(bootstrap_servers=bootstrap_servers) as producer,
        AIOKafkaConsumer(topic, bootstrap_servers=bootstrap_servers) as consumer,
    ):
        async for gain_table in leap_stream_gain_tables(
            str(MWA_MS),
            obs_directions=obs_directions,
            impl="cpu",
            verbosity=logging.WARNING
        ):
            gain_shape = gain_table.gain.shape
            residual_shape = (gain_shape[0], gain_shape[2], gain_shape[3], gain_shape[4])
            sdp_gain_table = GainTable.constructor(
                gain=gain_table.gain,
                time=gain_table.time,
                interval=gain_table.interval,
                weight=np.zeros(shape=gain_shape),
                residual=np.zeros(shape=residual_shape),
                frequency=gain_table.frequency,
                receptor_frame=None,
                phasecentre=gain_table.phasecentre,
                configuration=None,
                jones_type='G',
            )
            await producer.send_and_wait(topic=topic, value=encode(sdp_gain_table))
        __logger.info("mock data calibration complete.\n")

        # Read back messages until timeout
        while partition_records := await consumer.getmany(timeout_ms=100):
            for records in partition_records.values():
                for record in records:
                    __logger.info(decode(record.value))


def main():
    logging.basicConfig(
        format='[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s',
        datefmt="%Y-%m-%dT%H:%M:%S",
        level=logging.INFO
    )
    asyncio.run(amain())


if __name__ == "__main__":
    main()
