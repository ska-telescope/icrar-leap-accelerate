

import numpy as np
import os
from pathlib import Path

import casacore.tables as tables
import asyncio
import time

from astropy.coordinates import SkyCoord, FK5

import logging

from leap.functional import leap_stream_calibrations

__logger = logging.getLogger(__name__)

MWA_MS = Path(
    os.path.dirname(__file__),
    "../../testdata/mwa/1197638568-split.ms"
).resolve()


async def amain():
    ms_field = tables.table(str(MWA_MS/"FIELD"), readonly=True, ack=False)
    ms_phase_center: np.ndarray = ms_field.getcell("PHASE_DIR", 0)
    ms_units: str = ms_field.getcoldesc("PHASE_DIR")["keywords"]["QuantumUnits"]

    obs_phase_center = SkyCoord(ms_phase_center, frame=FK5, unit=ms_units)
    obs_directions = [
        SkyCoord(ra=7.09767229e-01, dec=-1.98773609e-04, frame=FK5, unit=ms_units),
        SkyCoord(ra=7.19767229e-01, dec=-1.28773609e-04, frame=FK5, unit=ms_units)
    ]

    # offsets: list[tuple[Angle, Angle]] = [
    #     obs_phase_center.spherical_offsets_to(obs_direction) for obs_direction in obs_directions
    # ]
    # print(offsets)

    async for calibration in leap_stream_calibrations(
        str(MWA_MS),
        obs_directions=obs_directions,
        impl="cpu",
        verbosity=logging.WARNING
    ):
        start_timestamp, end_timestamp = (calibration.start_epoch, calibration.end_epoch)
        __logger.info(
            "got solution for time interval %s -> %s",
            start_timestamp,
            end_timestamp
        )
        beam_calibrations = calibration.beam_calibrations
        for beam_calibration in beam_calibrations:
            __logger.info("direction: %s", beam_calibration.direction)
            __logger.debug("antenna_phases:\n%s", beam_calibration.antenna_phases)
        
        # some slow operation
        time.sleep(1)


def main():
    logging.basicConfig(
        format='[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s',
        level=logging.INFO
    )
    asyncio.run(amain())


if __name__ == "__main__":
    main()
